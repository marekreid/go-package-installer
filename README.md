# Go Package Installer

## Overview

This project is a Go program designed to read a list of package names from a text file and install them using the `brew` package manager. Currently, it only supports macOS but will be updated to include support for Arch Linux and Debian-based systems.

## Features

- Reads package names from a specified text file.
- Installs packages using `brew install`.
- Automatically retries with `brew install --cask` if the initial installation fails.
- Outputs installation progress to the terminal.

## Requirements

- macOS
- Homebrew (brew) installed
- Go installed (for building and running the program)

## Usage

### Building the Program

1. Clone the repository or download the source code.
2. Open a terminal and navigate to the project directory.
3. Build the program using the following command:

```sh
go build -o package-installer main.go
```

### Running the Program

To run the program, use the following command:

```sh
./package-installer <package-list.txt>
```
For testing you can use my minimal package list packaged with the repo at:
```sh
./package-installer ../package-list.txt
```

- `<package-list.txt>` is the path to the text file containing the list of package names to be installed. If no file is provided, it defaults to `package-list.txt`.

### Example

1. Create a text file named `package-list.txt` with the following content:

```
git
node
background-music
```

2. Run the program:

```sh
./package-installer package-list.txt
```

### Output

The program will print each package being installed and show the progress in the terminal. If a package requires `--cask`, the program will attempt to install it with `brew install --cask`.

## Refactoring Notice

This project is currently in a preliminary state and requires refactoring for improved maintainability and extensibility. Planned improvements include:

- Refactoring the code to better separate concerns and improve readability.
- Adding support for Arch Linux using `pacman` and AUR helpers.
- Adding support for Debian-based systems using `apt-get` and `dpkg`.

## Upcoming Features

- **Arch Linux Support**: Installing packages using `pacman` and AUR helpers.
- **Debian Support**: Installing packages using `apt-get` and `dpkg`.

## Contribution

Contributions are welcome! Feel free to submit issues and pull requests to help improve the project.

## License

This project is licensed under the MIT License.

## Contact

For any questions or suggestions, please reach out to [Your Email].

---

**Note**: This project is a work in progress and not yet fully functional for all intended platforms. Please use it with caution and report any issues you encounter.
