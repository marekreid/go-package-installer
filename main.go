package main

import (
	"fmt"
  "bufio"
	"os"
	"os/exec"
	"strings"
)

func main() {
	dependenciesFileName := checkPackageFile()
	fmt.Printf("Package list to use: %s\n", dependenciesFileName)

	packageList, err := readPackageListFromFile(dependenciesFileName)
	if err != nil {
		fmt.Println("Error reading package list:", err)
		return
	}

	fmt.Println("Package List:", packageList)

	installPackages(packageList)
}

func checkPackageFile() string {
	if len(os.Args) < 2 {
		fmt.Println("No provided package file - using default")
		return "package-list.txt"
	} else {
		fmt.Println("Using provided package file")
		return os.Args[1]
	}
}

func readPackageListFromFile(fileName string) ([]string, error) {
	file, err := os.Open(fileName)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	var packageList []string
	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanWords)

	fmt.Println("Scanning file for words...")

	for scanner.Scan() {
		word := checkPlusReplaceWithSpace(scanner.Text())
		fmt.Println(word)
		packageList = append(packageList, word)
	}

	if err := scanner.Err(); err != nil {
		return nil, err
	}

	return packageList, nil
}

func checkPlusReplaceWithSpace(p string) string {
	if strings.Contains(p, "+") {
		p = strings.Replace(p, "+", "-", -1)
		fmt.Println("Processed word: ", p)
	}
	return p
}

func installPackages(packageList []string) {
	for _, pkg := range packageList {
		err := runBrewInstall(pkg)
		if err != nil {
			fmt.Println("Initial command failed, trying --cask:", pkg)
			err = runBrewInstallWithCask(pkg)
			if err != nil {
				fmt.Println("Command failed with error:", err)
			}
		}
	}
}

func runBrewInstall(pkg string) error {
	command := fmt.Sprintf("brew install %s", pkg)
	fmt.Println("Executing command:", command)

	cmd := exec.Command("sh", "-c", command)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	if err := cmd.Start(); err != nil {
		return fmt.Errorf("error starting command: %w", err)
	}

	if err := cmd.Wait(); err != nil {
		return fmt.Errorf("command finished with error: %w", err)
	}

	return nil
}

func runBrewInstallWithCask(pkg string) error {
	command := fmt.Sprintf("brew install --cask %s", pkg)
	fmt.Println("Executing command:", command)

	cmd := exec.Command("sh", "-c", command)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	if err := cmd.Start(); err != nil {
		return fmt.Errorf("error starting command: %w", err)
	}

	if err := cmd.Wait(); err != nil {
		return fmt.Errorf("command finished with error: %w", err)
	}

	return nil
}

